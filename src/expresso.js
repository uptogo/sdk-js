'use strict';

import Pedido from './pedido';

class Expresso extends Pedido {
  constructor() {
    super();
  }

  enviar(callback) {
    this.uptogo.post('/Pedido.php/criar', {
      Pedido: this
    }, {
      headers: {'chave': this.apiKey}
    }).then(({data: pedido}) => {
      if (pedido.sucesso === true) {
        this.Id = pedido.id;
        callback(null, this);
      } else {
        callback('Erro ao criar pedido');
      }
    }).catch(erro => {
      callback('Erro ao enviar pedido');
    });
  }

  calcularRota(callback) {
    if (this.pontos.length !== 2) {
      callback('Número de pontos insuficiente');
    } else {
      this.maps.directions.route({
        origin: this.pontos[0].Endereco.getFormatado(),
        destination: this.pontos[1].Endereco.getFormatado(),
        travelMode: 'DRIVING'
      }, (resposta, status) => {
        if (status === 'OK') {
          if (resposta.routes.length > 0) {
            callback(null, resposta.routes[0]);
          } else {
            callback('Nenhuma rota encontrada');
          }
        } else {
          callback('Erro ao calcular rota');
        }
      });
    }
  }

  calcularDistancia(rota, callback) {
    let distancia = rota.legs.reduce((a, b) => a + b.distance.value, 0);
    if (distancia === 0) {
      callback('Erro ao calcular distância');
    } else {
      distancia /= 1000;
      distancia = parseFloat(distancia.toFixed(2));
      callback(null, distancia);
    }
  }

  calcularTempo(rota, callback) {
    let tempo = rota.legs.reduce((a, b) => a + b.duration.value, 0);
    if (tempo === 0) {
      callback('Erro ao calcular tempo');
    } else {
      tempo /= 60;
      tempo = parseFloat(tempo.toFixed(2));
      callback(null, tempo);
    }
  }

  calcularValor(callback) {
    if (this.apiKey) {
      this.uptogo.post('/Admin.php/getValores', {}, {
        headers: {'chave': this.apiKey}
      }).then(({data: baseCalculo}) => {
        let valor = parseFloat((
          (baseCalculo.unitarioMinuto * this.Tempo)
          +
          (baseCalculo.unitarioKm * this.Distancia)
          +
          baseCalculo.valorPontoExpresso
        ).toFixed(2));
        if (valor < baseCalculo.minimo) {
          valor = baseCalculo.minimo;
        }
        callback(null, valor);
      }).catch(erro => {
        callback('Erro ao obter base de cálculo');
      });
    } else {
      callback('Chave da API ausente');
    }
  }

  calcular(callback) {
    this.calcularRota((erro, rota) => {
      if (!erro) {
        this.calcularDistancia(rota, (erro, distancia) => {
          if (!erro) {
            this.Distancia = distancia;
            this.calcularTempo(rota, (erro, tempo) => {
              if (!erro) {
                this.Tempo = tempo;
                this.calcularValor((erro, valor) => {
                  if (!erro) {
                    this.calcularDesconto(valor, (erro, valor) => {
                      if (!erro) {
                        this.Valor = valor;
                        callback(null, this);
                      } else {
                        callback(erro);
                      }
                    });                    
                  } else {
                    callback(erro);
                  }
                });
              } else {
                callback(erro);
              }
            })
          } else {
            callback(erro);
          }
        })
      } else {
        callback(erro);
      }
    });
  }
}

export default Expresso;