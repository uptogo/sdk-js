'use strict';

class Endereco {
  constructor(mapsAddressComponents) {
    for (let i = 0; i < mapsAddressComponents.length; i++) {
      let tipo = mapsAddressComponents[i].types[0];
      if (tipo === 'route') {
        this.Logradouro = mapsAddressComponents[i]['long_name'];
      } else if (tipo === 'postal_code') {
        this.Cep = mapsAddressComponents[i]['long_name'].replace('-', '');
      } else if (tipo === 'street_number') {
        this.Numero = mapsAddressComponents[i]['long_name'];
      } else if (tipo === 'political') {
        this.Bairro = mapsAddressComponents[i]['long_name'];
      } else if (tipo === 'administrative_area_level_2') {
        this.Cidade = mapsAddressComponents[i]['long_name'];
      } else if (tipo === 'administrative_area_level_1') {
        this.Estado = mapsAddressComponents[i]['short_name'];
      } else if (tipo === 'country') {
        this.pais = mapsAddressComponents[i]['short_name'];
      }
    }
  }

  getFormatado() {
    return `${this.Logradouro}, ${this.Numero} - ${this.Bairro} - ${this.Cep} - ${this.Cidade}/${this.Estado}`;
  }
}

export default Endereco;