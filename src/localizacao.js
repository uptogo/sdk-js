'use strict';

class Localizacao {
  constructor(mapsLocation) {
    this.Latitude = mapsLocation.lat();
    this.Longitude = mapsLocation.lng();
  }
}

export default Localizacao;