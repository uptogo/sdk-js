'use strict';

import Pedido from './pedido';

const math = require('mathjs');

class Ecommerce extends Pedido {
  constructor() {
    super();    
  }

  enviar(callback) {
    this.localCliente = {
      lat: this.pontos[0].Localizacao.Latitude,
      lng: this.pontos[0].Localizacao.Longitude
    };
    this.pontos.splice(0, 1);
    this.uptogo.post('/Pedido.php/criarEcommerce', {
      Pedido: this
    }, {
      headers: {'chave': this.apiKey}
    }).then(({data: pedido}) => {
      if (pedido.sucesso === true) {
        this.Id = pedido.id;
        callback(null, this);
      } else {
        callback('Erro ao criar pedido');
      }
    }).catch(erro => {
      callback('Erro ao enviar pedido');
    });
  }

  deg2rad(deg) {
    return deg * (Math.PI/180);
  }

  calcularDistancia(callback) {
    let lat1 = -23.5502351;
    let lon1 = -46.633981;
    let lat2 = this.pontos[1].Localizacao.Latitude;
    let lon2 = this.pontos[1].Localizacao.Longitude;
    let R = 6371;
    let dLat = this.deg2rad(lat2-lat1);
    let dLon = this.deg2rad(lon2-lon1);
    let a =
      math.sin(dLat/2) * math.sin(dLat/2) +
      math.cos(this.deg2rad(lat1)) * math.cos(this.deg2rad(lat2)) *
      math.sin(dLon/2) * math.sin(dLon/2)
    ;
    let c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a));
    let d = R * c;
    if (d === 0) {
      callback('Erro ao calcular distância');
    } else {
      callback(null, d);
    }
  }

  calcularTempo(callback) {
    callback(null, 1440);
  }

  calcularValor(callback) {
    if (this.apiKey) {
      this.uptogo.post('/Admin.php/getValores', {}, {
        headers: {'chave': this.apiKey}
      }).then(({data: baseCalculo}) => {
        let valor = baseCalculo.valorPontoEcommerce;
        if (this.Distancia > baseCalculo.raioEcommerce) {
          valor += (this.Distancia - baseCalculo.raioEcommerce) * baseCalculo.taxaKmExcedidoEcommerce;
        }
        callback(null, valor);
      }).catch(erro => {
        callback('Erro ao obter base de cálculo');
      });
    } else {
      callback('Chave da API ausente');
    }
  }

  calcular(callback) {
    this.calcularDistancia((erro, distancia) => {
      if (!erro) {
        this.Distancia = distancia;
        this.calcularTempo((erro, tempo) => {
          if (!erro) {
            this.Tempo = tempo;
            this.calcularValor((erro, valor) => {
              if (!erro) {
                this.calcularDesconto(valor, (erro, valor) => {
                  this.Valor = valor;
                  callback(null, this);
                });
              } else {
                callback(erro);
              }
            });
          } else {
            callback(erro);
          }
        });
      } else {
        callback(erro);
      }
    });
  }
}

export default Ecommerce;