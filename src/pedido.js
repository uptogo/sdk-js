'use strict';

import Ponto from './ponto';

class Pedido {
  constructor() {
    this.pontos = [];
    this.objetos = [];
    this.MetodoPagamento = 3;
    this.OtimizarRota = false;
    this.SenhaRastreamento = (new Date()).getTime().toString();
    this.uptogo = require('axios').create({
      baseURL: 'https://uptogo.com.br/app/api'
    });
  }

  setApiKey(apiKey, callback) {
    this.apiKey = apiKey;
    this.uptogo.post('/Cliente.php/getEndereco', {
      apiKey: this.apiKey
    }, {
      headers: {'chave': this.apiKey}
    }).then(({data: endereco}) => {
      this.setOrigem(
        `${endereco.Logradouro}, ${endereco.Numero} - ${endereco.Bairro} - ${endereco.Cep} - ${endereco.Cidade}/${endereco.Estado}`,
        callback
      );
    }).catch(erro => {
      callback('Erro ao obter endereço através da chave da API');
    });
  }

  setGoogleMapsApiKey(googleMapsApiKey, callback) {
    require('google-maps-api')(googleMapsApiKey)()
    .then(maps => {
      this.maps = {
        geocoder: new maps.Geocoder(),
        directions: new maps.DirectionsService()
      };
      callback(null, this);
    }).catch(erro => 
      callback('Erro ao definir a chave da API do Google Maps')
    );
  }

  setCupomDesconto(cupomDesconto) {
    this.CupomDesconto = cupomDesconto;
    return this;
  }

  setOrigem(endereco, callback) {
    this.maps.geocoder.geocode({
      address: endereco
    }, (enderecos, status) => {
      if (status === 'OK') {
        if (enderecos.length > 0) {
          this.pontos[0] = new Ponto(enderecos[0]);
          this.pontos[0].setLabel('A');
          this.pontos[0].setTarefa('Manipular objeto de loja virtual');
          callback(null, this);
        } else {
          callback('Nenhum endereço encontrado');
        }
      } else {
        callback('Verifique a chave da API do Google Maps');
      }
    });
  }

  setDestino(endereco, callback) {
    this.maps.geocoder.geocode({
      address: endereco
    }, (enderecos, status) => {
      if (status === 'OK') {
        if (enderecos.length > 0) {
          this.pontos[1] = new Ponto(enderecos[0]);
          this.pontos[1].setLabel('B');
          this.pontos[1].setTarefa('Manipular objeto de loja virtual');
          callback(null, this);
        } else {
          callback('Nenhum endereço encontrado');
        }
      } else {
        callback('Verifique a chave da API do Google Maps');
      }
    });
  }

  calcularDesconto(valor, callback) {
    if (this.CupomDesconto) {
      this.uptogo.post('/Admin.php/validarCodigo', {
        tipo: 1,
        valor: this.CupomDesconto
      }, {
        headers: {'chave': this.apiKey}
      }).then(({data: cupom}) => {
        if (cupom.valido === true) {
          valor -= valor * cupom.desconto;
        }
        callback(null, valor);
      }).catch(erro => {
        callback('Erro ao validar cupom de desconto');
      });
    } else {
      callback(null, valor);
    }
  }
}

export default Pedido;