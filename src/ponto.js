'use strict';

import Endereco from './endereco';
import Localizacao from './localizacao';

class Ponto {
  constructor(mapsResult) {
    this.Localizacao = new Localizacao(mapsResult.geometry.location);
    this.Endereco = new Endereco(mapsResult.address_components);
  }

  setLabel(label) {
    this.Label = label;
  }

  setTarefa(tarefa) {
    this.Tarefa = tarefa;
  }
}

export default Ponto;